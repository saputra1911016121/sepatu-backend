<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function all(Request $request)
    {
        $id = $request->input('id');
        $limit = $request->input('limit');
        $name = $request->input('id');
        $description = $request->input('description');
        $tags = $request->input('tags');
        $categories = $request->input('categories');

        $price_from = $request->input('price_from');
        $price_to = $request->input('price_to');

        //ini untuk id

        if ($id)
        {
            $product = Product::with(['category', 'galleries'])->find($id);

            if ($product){
                return ResponseFormatter::success(
                    $product,
                    'Data Produk Berhasil'
                );
            }
            else{
                return ResponseFormatter::error(
                    null,
                    'Data Produk Kosong',
                    404
                );
            }
        }

        $product = Product::with('category', 'galleries');

        //ini untuk nama
        if ($name)
        {
            $product->where('name', 'like', '%' . $name . '%');
        }

        //ini untuk deskripsi
        if ($description)
        {
            $product->where('name', 'like', '%' . $name . '%');
        }

        //ini untuk tags
        if ($tags)
        {
            $product->where('name', 'like', '%' . $name . '%');
        }

        //ini untuk price_from
        if ($price_from)
        {
            $product->where('price', '>=', $price_from);
        }

        //ini untuk price_to
        if ($price_to)
        {
            $product->where('price', '<=', $price_to);
        }

        //ini untuk categories
        if ($categories)
        {
            $product->where('categories', $categories);
        }

        return ResponseFormatter::success(
            $product->paginate($limit),
            'data berhasil'
        );

    }
}
